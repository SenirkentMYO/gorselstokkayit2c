﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace StokTakip
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void hesaplarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void kullanıcıGirişiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Kullanici Kul = new Kullanici();
            Kul.Show();

        }

        private void yöneticiGirişiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Yonetici ynt = new Yonetici();
            ynt.Show();


        }

        private void Form1_Load(object sender, EventArgs e)
        {

      
            this.CenterToScreen();

        }

        private void stokEkleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StokEkleme stk = new StokEkleme();
            stk.Show();
        }

        private void stokTkipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StokTakip stokt = new StokTakip();
            stokt.Show();

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult Kapat;
            Kapat = MessageBox.Show("Programdan Çıkmak İstiyormusunuz ?", "Uyarı !", MessageBoxButtons.YesNo);

            if (Kapat == DialogResult.Yes)
            {
                MessageBox.Show("ANA PROGRAM KAPATILIYOR.");
            }
            else if (Kapat == DialogResult.No)
            {
                ActiveForm.ShowDialog();

            }
        }
    }
}
