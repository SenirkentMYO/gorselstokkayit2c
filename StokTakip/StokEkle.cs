﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StokTakip
{
    public partial class StokEkleme : Form
    {
        public StokEkleme()
        {
            InitializeComponent();
        }

        private void StokEkleme_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void StokEkleme_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult Kapat;
            Kapat = MessageBox.Show("Programdan Çıkmak İstiyormusunuz ?", "Uyarı !", MessageBoxButtons.YesNo);

            if (Kapat == DialogResult.Yes)
            {
                MessageBox.Show("Program Kapatılıyor.");
            }
            else if (Kapat == DialogResult.No)
            {
                ActiveForm.ShowDialog();

            }
        }
    }
}
