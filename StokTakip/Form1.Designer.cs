﻿namespace StokTakip
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.kullanıcıPaneliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kullanıcıGirişiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yöneticiGirişiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stokEkleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stokSilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hesaplarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nakitEklemeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nakitÇıkarmaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nakitHareketleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayarlarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.güvenlikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yedekleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stokTkipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kullanıcıPaneliToolStripMenuItem,
            this.stokEkleToolStripMenuItem,
            this.stokSilToolStripMenuItem,
            this.hesaplarToolStripMenuItem,
            this.ayarlarToolStripMenuItem,
            this.stokTkipToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(484, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // kullanıcıPaneliToolStripMenuItem
            // 
            this.kullanıcıPaneliToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kullanıcıGirişiToolStripMenuItem,
            this.yöneticiGirişiToolStripMenuItem});
            this.kullanıcıPaneliToolStripMenuItem.Name = "kullanıcıPaneliToolStripMenuItem";
            this.kullanıcıPaneliToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.kullanıcıPaneliToolStripMenuItem.Text = "Kullanıcı Paneli";
            // 
            // kullanıcıGirişiToolStripMenuItem
            // 
            this.kullanıcıGirişiToolStripMenuItem.BackColor = System.Drawing.Color.Lime;
            this.kullanıcıGirişiToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.kullanıcıGirişiToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.kullanıcıGirişiToolStripMenuItem.Name = "kullanıcıGirişiToolStripMenuItem";
            this.kullanıcıGirişiToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.kullanıcıGirişiToolStripMenuItem.Text = "Kullanıcı Girişi";
            this.kullanıcıGirişiToolStripMenuItem.Click += new System.EventHandler(this.kullanıcıGirişiToolStripMenuItem_Click);
            // 
            // yöneticiGirişiToolStripMenuItem
            // 
            this.yöneticiGirişiToolStripMenuItem.BackColor = System.Drawing.Color.Lime;
            this.yöneticiGirişiToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.yöneticiGirişiToolStripMenuItem.Name = "yöneticiGirişiToolStripMenuItem";
            this.yöneticiGirişiToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.yöneticiGirişiToolStripMenuItem.Text = "Yönetici Girişi";
            this.yöneticiGirişiToolStripMenuItem.Click += new System.EventHandler(this.yöneticiGirişiToolStripMenuItem_Click);
            // 
            // stokEkleToolStripMenuItem
            // 
            this.stokEkleToolStripMenuItem.Name = "stokEkleToolStripMenuItem";
            this.stokEkleToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.stokEkleToolStripMenuItem.Text = "Stok Ekle";
            this.stokEkleToolStripMenuItem.Click += new System.EventHandler(this.stokEkleToolStripMenuItem_Click);
            // 
            // stokSilToolStripMenuItem
            // 
            this.stokSilToolStripMenuItem.Name = "stokSilToolStripMenuItem";
            this.stokSilToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.stokSilToolStripMenuItem.Text = "Stok Sil";
            // 
            // hesaplarToolStripMenuItem
            // 
            this.hesaplarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nakitEklemeToolStripMenuItem,
            this.nakitÇıkarmaToolStripMenuItem,
            this.nakitHareketleriToolStripMenuItem});
            this.hesaplarToolStripMenuItem.Name = "hesaplarToolStripMenuItem";
            this.hesaplarToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.hesaplarToolStripMenuItem.Text = "Hesaplar";
            this.hesaplarToolStripMenuItem.Click += new System.EventHandler(this.hesaplarToolStripMenuItem_Click);
            // 
            // nakitEklemeToolStripMenuItem
            // 
            this.nakitEklemeToolStripMenuItem.Name = "nakitEklemeToolStripMenuItem";
            this.nakitEklemeToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.nakitEklemeToolStripMenuItem.Text = "Nakit Ekleme";
            // 
            // nakitÇıkarmaToolStripMenuItem
            // 
            this.nakitÇıkarmaToolStripMenuItem.Name = "nakitÇıkarmaToolStripMenuItem";
            this.nakitÇıkarmaToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.nakitÇıkarmaToolStripMenuItem.Text = "Nakit Çıkarma";
            // 
            // nakitHareketleriToolStripMenuItem
            // 
            this.nakitHareketleriToolStripMenuItem.Name = "nakitHareketleriToolStripMenuItem";
            this.nakitHareketleriToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.nakitHareketleriToolStripMenuItem.Text = "Nakit Hareketleri";
            // 
            // ayarlarToolStripMenuItem
            // 
            this.ayarlarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.güvenlikToolStripMenuItem,
            this.yedekleToolStripMenuItem});
            this.ayarlarToolStripMenuItem.Name = "ayarlarToolStripMenuItem";
            this.ayarlarToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.ayarlarToolStripMenuItem.Text = "Ayarlar";
            // 
            // güvenlikToolStripMenuItem
            // 
            this.güvenlikToolStripMenuItem.Name = "güvenlikToolStripMenuItem";
            this.güvenlikToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.güvenlikToolStripMenuItem.Text = "Güvenlik";
            // 
            // yedekleToolStripMenuItem
            // 
            this.yedekleToolStripMenuItem.Name = "yedekleToolStripMenuItem";
            this.yedekleToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.yedekleToolStripMenuItem.Text = "Yedekle";
            // 
            // stokTkipToolStripMenuItem
            // 
            this.stokTkipToolStripMenuItem.Name = "stokTkipToolStripMenuItem";
            this.stokTkipToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.stokTkipToolStripMenuItem.Text = "Stok Takip";
            this.stokTkipToolStripMenuItem.Click += new System.EventHandler(this.stokTkipToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::StokTakip.Properties.Resources.photoshop_arka_plan_3;
            this.ClientSize = new System.Drawing.Size(484, 382);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Stok Kayıt Ekranı";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem kullanıcıPaneliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kullanıcıGirişiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yöneticiGirişiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stokEkleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stokSilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hesaplarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayarlarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nakitEklemeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nakitÇıkarmaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nakitHareketleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem güvenlikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yedekleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stokTkipToolStripMenuItem;
    }
}

