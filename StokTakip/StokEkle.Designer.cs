﻿namespace StokTakip
{
    partial class StokEkleme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.urun = new System.Windows.Forms.TextBox();
            this.mevcut = new System.Windows.Forms.TextBox();
            this.tarih = new System.Windows.Forms.TextBox();
            this.alinan = new System.Windows.Forms.TextBox();
            this.alsfiyati = new System.Windows.Forms.TextBox();
            this.stsfiyati = new System.Windows.Forms.TextBox();
            this.gelen = new System.Windows.Forms.TextBox();
            this.toplam = new System.Windows.Forms.TextBox();
            this.odenen = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.kategori = new System.Windows.Forms.Button();
            this.stkekle = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // urun
            // 
            this.urun.Location = new System.Drawing.Point(92, 33);
            this.urun.Name = "urun";
            this.urun.Size = new System.Drawing.Size(153, 20);
            this.urun.TabIndex = 0;
            // 
            // mevcut
            // 
            this.mevcut.Location = new System.Drawing.Point(92, 59);
            this.mevcut.Name = "mevcut";
            this.mevcut.Size = new System.Drawing.Size(153, 20);
            this.mevcut.TabIndex = 0;
            // 
            // tarih
            // 
            this.tarih.Location = new System.Drawing.Point(92, 85);
            this.tarih.Name = "tarih";
            this.tarih.Size = new System.Drawing.Size(153, 20);
            this.tarih.TabIndex = 0;
            // 
            // alinan
            // 
            this.alinan.Location = new System.Drawing.Point(92, 111);
            this.alinan.Name = "alinan";
            this.alinan.Size = new System.Drawing.Size(153, 20);
            this.alinan.TabIndex = 0;
            // 
            // alsfiyati
            // 
            this.alsfiyati.Location = new System.Drawing.Point(92, 137);
            this.alsfiyati.Name = "alsfiyati";
            this.alsfiyati.Size = new System.Drawing.Size(153, 20);
            this.alsfiyati.TabIndex = 0;
            // 
            // stsfiyati
            // 
            this.stsfiyati.Location = new System.Drawing.Point(92, 163);
            this.stsfiyati.Name = "stsfiyati";
            this.stsfiyati.Size = new System.Drawing.Size(153, 20);
            this.stsfiyati.TabIndex = 0;
            // 
            // gelen
            // 
            this.gelen.Location = new System.Drawing.Point(92, 189);
            this.gelen.Name = "gelen";
            this.gelen.Size = new System.Drawing.Size(153, 20);
            this.gelen.TabIndex = 0;
            // 
            // toplam
            // 
            this.toplam.Location = new System.Drawing.Point(92, 215);
            this.toplam.Name = "toplam";
            this.toplam.Size = new System.Drawing.Size(153, 20);
            this.toplam.TabIndex = 0;
            // 
            // odenen
            // 
            this.odenen.Location = new System.Drawing.Point(92, 241);
            this.odenen.Name = "odenen";
            this.odenen.Size = new System.Drawing.Size(153, 20);
            this.odenen.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ürün Adı";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mevcut Stok";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Tarih";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Alınan Firma";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Alış Fiyatı";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Satış Fiyatı";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Gelen Miktar";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Toplam Tutar";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 244);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Ödenen Miktar";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 273);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Kategoriler";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(2, 293);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 1;
            // 
            // kategori
            // 
            this.kategori.Location = new System.Drawing.Point(92, 267);
            this.kategori.Name = "kategori";
            this.kategori.Size = new System.Drawing.Size(51, 23);
            this.kategori.TabIndex = 2;
            this.kategori.Text = "Seç";
            this.kategori.UseVisualStyleBackColor = true;
            // 
            // stkekle
            // 
            this.stkekle.Location = new System.Drawing.Point(159, 267);
            this.stkekle.Name = "stkekle";
            this.stkekle.Size = new System.Drawing.Size(75, 23);
            this.stkekle.TabIndex = 3;
            this.stkekle.Text = "Stok Ekle";
            this.stkekle.UseVisualStyleBackColor = true;
            // 
            // StokEkleme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 345);
            this.Controls.Add(this.stkekle);
            this.Controls.Add(this.kategori);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.odenen);
            this.Controls.Add(this.toplam);
            this.Controls.Add(this.gelen);
            this.Controls.Add(this.stsfiyati);
            this.Controls.Add(this.alsfiyati);
            this.Controls.Add(this.alinan);
            this.Controls.Add(this.tarih);
            this.Controls.Add(this.mevcut);
            this.Controls.Add(this.urun);
            this.Name = "StokEkleme";
            this.Text = "StokEkle";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StokEkleme_FormClosed);
            this.Load += new System.EventHandler(this.StokEkleme_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox urun;
        private System.Windows.Forms.TextBox mevcut;
        private System.Windows.Forms.TextBox tarih;
        private System.Windows.Forms.TextBox alinan;
        private System.Windows.Forms.TextBox alsfiyati;
        private System.Windows.Forms.TextBox stsfiyati;
        private System.Windows.Forms.TextBox gelen;
        private System.Windows.Forms.TextBox toplam;
        private System.Windows.Forms.TextBox odenen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button kategori;
        private System.Windows.Forms.Button stkekle;
    }
}