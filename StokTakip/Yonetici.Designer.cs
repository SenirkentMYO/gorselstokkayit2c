﻿namespace StokTakip
{
    partial class Yonetici
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.yon = new System.Windows.Forms.TextBox();
            this.psw = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // yon
            // 
            this.yon.Location = new System.Drawing.Point(77, 45);
            this.yon.Name = "yon";
            this.yon.Size = new System.Drawing.Size(100, 20);
            this.yon.TabIndex = 0;
            this.yon.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // psw
            // 
            this.psw.Location = new System.Drawing.Point(77, 71);
            this.psw.Name = "psw";
            this.psw.PasswordChar = '*';
            this.psw.Size = new System.Drawing.Size(100, 20);
            this.psw.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Yönetici Adı :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Parola :";
            // 
            // grs
            // 
            this.grs.Location = new System.Drawing.Point(77, 97);
            this.grs.Name = "grs";
            this.grs.Size = new System.Drawing.Size(100, 23);
            this.grs.TabIndex = 4;
            this.grs.Text = "Giriş";
            this.grs.UseVisualStyleBackColor = true;
            this.grs.Click += new System.EventHandler(this.grs_Click);
            // 
            // Yonetici
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 203);
            this.Controls.Add(this.grs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.psw);
            this.Controls.Add(this.yon);
            this.Name = "Yonetici";
            this.Text = "Yonetici";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Yonetici_FormClosed);
            this.Load += new System.EventHandler(this.Yonetici_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox yon;
        private System.Windows.Forms.TextBox psw;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button grs;
    }
}